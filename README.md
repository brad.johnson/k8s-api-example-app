# API Example App
#### Do not install this in a production cluster
This is a demo application and may use namespaces you already have in use.

### Requirements
* A fresh development kubernetes cluster of some type, like [minikube](https://minikube.sigs.k8s.io/docs/start/)
* kubectl

### Ways this could be improved
* Unit tests
* Load testing
* Kubernetes yaml check/linting in CI
* Automated MR generation for upgrading image after merge to main
* Multiple environments
* Review environment/namespace on MR pipeline
* Alerts
* Hook up with cloud provider and terraform

### Minikube setup
```shell
$ minikube delete && minikube start --kubernetes-version=v1.24.0 --memory=6g --bootstrapper=kubeadm --extra-config=kubelet.authentication-token-webhook=true --extra-config=kubelet.authorization-mode=Webhook --extra-config=scheduler.bind-address=0.0.0.0 --extra-config=controller-manager.bind-address=0.0.0.0

```

### Initial Bootstrapping
ArgoCD will manage itself. The setup is fully declarative from code.

1) Create namespace and run initial kustomize apply
```shell
kubectl create namespace argocd
cd deploy/argocd
# Apply CRDs first to avoid errors with missing APIs
kubectl apply -k https://github.com/argoproj/argo-cd/manifests/crds\?ref\=stable
kustomize build . | kubectl apply -f -
```
2) ArgoCD comes online and now resyncs from this same repo 
3) Any updates may now be simply committed and Argo deals with the updates



### View ArgoCD web UI with default creds
```shell
# Get initial generated admin password
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 --decode
# Start port forwarding
kubectl port-forward -n argocd service/argocd-server 8080:80 
```
Open in web browser:

Username: admin

Password: (see above)

http://localhost:8080


### Connect to example app and inspect endpoints
```shell
kubectl port-forward -n api-example-test service/api-example 8081:80
curl localhost:8081
curl localhost:8081/ready
curl localhost:8081/metrics
curl localhost:8081/api/v1/test
curl localhost:8081/api/v1/long
```

### See dashboard for example app
```shell
kubectl port-forward -n observe service/kube-prometheus-stack-grafana 8082:80
```
Open dashboard in web browser:

Username: admin

Password: prom-operator

http://localhost:8082/d/_example/api-example-app?orgId=1&refresh=5s

### See prometheus dashboard and scraping config
```shell
kubectl port-forward -n observe service/kube-prometheus-stack-prometheus 8083:80
```
Open in web browser: http://localhost:8083

### Teardown
```shell
minikube delete
```
