#!/usr/bin/env python3
import flask
from prometheus_flask_exporter import PrometheusMetrics
from flask import request, jsonify
import time

app = flask.Flask(__name__)
# app.config["DEBUG"] = True
metrics = PrometheusMetrics(app)

# Version metric
__version__ = 1.0
metrics.info("version", "App version", version="__version__")

metrics.register_default(
    metrics.counter(
        "by_path_counter",
        "Request count by request paths",
        labels={"path": lambda: request.path},
    )
)


@app.route("/", methods=["GET"])
def home():
    return """
    <h1>API Example App</h1>
    <p>Example that responds on /health /metrics /api/v1/test and /api/v1/long</p>
    """


@app.route("/health", methods=["GET"])
def health():
    return "success"


@app.route("/api/v1/test", methods=["GET"])
def api_test():
    return jsonify({"test": "success"})


@app.route("/api/v1/long")
@metrics.gauge("long_request_in_progress", "Long requests in progress")
def long():
    time.sleep(20)
    return jsonify({"test": "long success"})


@app.errorhandler(404)
def not_found(e):
    return "<h1>404</h1><p>Nothing to see here</p>", 404


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)
